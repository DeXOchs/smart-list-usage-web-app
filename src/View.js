import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Carousel from 'react-bootstrap/Carousel';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleLeft } from '@fortawesome/free-solid-svg-icons'
import './style.css';

export default class View extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentIndex: 0,
      direction: null,
      viewDetail: false,
    }
  }

  componentDidMount() {
    this.props.liftRight(this.viewRight);
    this.props.liftLeft(this.viewLeft);
    this.props.liftBottom(() => {
      this.doPreferredAction();
      this.chooseCurrent();
    })
    this.props.liftTop(this.leaveCurrent);
  }

  render() {
    let content;
    if (this.props.usableList == null || this.props.usableList === []) {
      content = (
        <div>
          <span className="lds-dual-ring"></span>
        </div>
      );
    }

    else if (this.state.viewDetail) {
      const currentListItem = (this.props.usableList.getList())[this.state.currentIndex];
      content = (
        <div className="d-flex flex-wrap col-12 col-xl-8 m-0 p-0 my-md-3 p-lg-3 rounded background-blueish-darken">
          <div className="d-block col-12 col-lg-6 p-0 rounded">
            <figure className="m-0 h-100">
              <img
                className="h-100 col-12 p-0 rounded border-bottom-reddish-orange object-fit-cover"
                src={currentListItem.getHeaderImageUrl()}
                alt={currentListItem.getHeaderImageUrl()}
              />
              <figcaption
                className="pt-2 pl-2 pb-5 pr-5 text-left figure-caption rounded"
                onClick={() => this.leaveCurrent()}
              >
                <FontAwesomeIcon icon={faAngleLeft}/>
              </figcaption>
            </figure>
          </div>
          <div className="d-flex flex-column d-block col-12 col-lg-6 my-3 my-lg-0 px-3 px-lg-0 pl-lg-3 justify-content-between">
            <div>
              <h3>{currentListItem.getTitle()}</h3>
              <p className="mt-3 text-justify" ><strong>
                {currentListItem.getTeaser()}
              </strong></p>
              <p className="text-justify">
                {currentListItem.getContent()
                  .split('\n')
                  .map((element) => <span>{element}<br/></span>)}
              </p>
            </div>
            <div className="d-flex justify-content-end">
              <button
                className='btn btn-lg btn-reddish-orange'
                onClick={() => this.doPreferredAction()}
              >
                {this.props.usableList.getActionDescription()}
              </button>
            </div>
          </div>
        </div>
      );

    } else {
      let listItems = this.props.usableList.getList().map((currentListItem, key) => (
        <Carousel.Item
          key={key}
          className="mw-90 h-80 rounded border-bottom-reddish-orange"
          onClick={() => this.chooseCurrent()}
        >
          <img
            className="rounded w-100 h-100 object-fit-cover"
            src={currentListItem.getHeaderImageUrl()}
            alt={currentListItem.getHeaderImageUrl ()}
            width="100%"
          />
          <Carousel.Caption className="carousel-caption w-100" >
            <h3>{currentListItem.getTitle()}</h3>
          </Carousel.Caption>
        </Carousel.Item>
      ));
      content = (
          <Row className="mx-3">
            <Carousel
              activeIndex={this.state.currentIndex}
              onSelect={this.handleSelect}
              interval={0}
              indicators={true}
            >
              {listItems}
            </Carousel>
          </Row>
      );
    }
    return (
      <Container className="d-flex w-100 p-0 justify-content-center">
          {content}
      </Container>
    );
  }

  handleSelect = (selectedIndex, e) => {
    this.setState((state, props) => ({
      currentIndex: selectedIndex,
      direction: e.direction
    }));
  }

  viewRight = () => {
    if (!this.state.viewDetail) {
      console.log('view right');
      this.handleSelect(
        (this.state.currentIndex + 1) % this.props.usableList.getList().length,
        'next'
      );
    }
  }

  viewLeft = () => {
    if (!this.state.viewDetail) {
      console.log('view left');
      this.handleSelect(
        ((this.state.currentIndex - 1) + this.props.usableList.getList().length) % this.props.usableList.getList().length,
        'prev'
      );
    }
  }

  chooseCurrent = () => {
    if (!this.state.viewDetail) {
      console.log('chooseCurrent');
      this.setState((state, props) => ({ viewDetail: !state.viewDetail }));
    }
  }

  leaveCurrent = () => {
    console.log('leaveCurrent');
    if (this.state.viewDetail) {
      this.setState((state, props) => ({ viewDetail: !state.viewDetail }));
    }
  }

  doPreferredAction = () => {
    if (this.state.viewDetail) {
      console.log('doPreferredAction');
      (this.props.usableList.getList())[this.state.currentIndex].doPreferredAction();
    }
  }
}
