import React from 'react';
import './style.css';
import View from './View';
import OrientationHandler from './OrientationHandler';
import StatefulGestureHandler from './StatefulGestureHandler';
import Service from './Service';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      displayPermissionRequest: this.shouldDisplayPermissionRequest(),
      usableList: null,
    };
  }

  componentDidMount() {
    this.service = new Service();
    if (this.getRoute() === 'imagedemo') {
      this.service.getDemoImages()
        .then(usableList => {
          this.setState({ usableList: usableList })
        });
    }
    else if (this.getRoute() === 'newsdemo') {
      this.service.getLatestNews()
        .then(usableList => {
          this.setState({ usableList: usableList })
        });
    } else {
      this.setState({
        usableList: this.service.getDummyContent()
      });
    }

    this.orientationHandler = new OrientationHandler();

    this.horizontalAxisGestureHandler = new StatefulGestureHandler({
      buffersize: 6,
      activationThreshold: 120
    });
    this.verticalAxisGestureHandler = new StatefulGestureHandler({
      buffersize: 6,
      activationThreshold: 120
    });
  }

  render() {
    let permissionRequest = null;
    if (this.state.displayPermissionRequest) {
      permissionRequest = (
        <div className="fixed-top position-absolute d-flex flex-column w-100 h-100 px-3 px-lg-5 justify-content-center background-blueish-black">
          <button
            className="d-block align-self-center btn btn-lg btn-block btn-success"
            onClick={this.requestDeviceMotionTracking}
          >
            Tap to enable motion recognition
          </button>
          <button
            className="d-block align-self-center btn btn-lg btn-block btn-outline-danger"
            onClick={this.declineDeviceMotionTracking}
          >
            Decline motion recognition
          </button>
        </div>
      );
    }
    return (
      <div className="App">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossOrigin="anonymous" />
        <div>
          {permissionRequest}
          <View className="View"
            usableList={this.state.usableList}
            liftRight={event => this.liftRight = event}
            liftLeft={event => this.liftLeft = event}
            liftBottom={event => this.liftBottom = event}
            liftTop={event => this.liftTop = event}
          />
        </div>
      </div>
    );
  }

  shouldDisplayPermissionRequest = () => {
    if (navigator.userAgent.includes('iPad; CPU OS 12_4_5') ||
        navigator.userAgent.includes('iPhone; CPU iPhone OS 13_2_3')) {
          return true;
    } else {
      return false;
    }
    /*
     * use this to determine if user agent is from iOS 13.
     *
    if (userAgent !== null) {
      let version = userAgent
        .substring(userAgent.indexOf("iPhone OS") + 10)
        .substring(0,2);
      version = parseInt(version);
      if (version >= 13) return true;
    }
    */
  }

  requestDeviceMotionTracking = () => {
    if (typeof(DeviceMotionEvent.requestPermission) !== undefined) {
      if (typeof DeviceMotionEvent.requestPermission === 'function') {
        DeviceMotionEvent.requestPermission()
          .then(response => {
            if (response === 'granted') {
              window.addEventListener('devicemotion', this.handleMotionEvent, true);
            }
            this.setState((state, props) => ({ displayPermissionRequest: false }));
          })
          .catch(console.error);
      } else {
        window.addEventListener('devicemotion', this.handleMotionEvent, true);
        this.setState((state, props) => ({ displayPermissionRequest: false }));
      }

    } else {
      alert('DeviceMotionEvent is not defined');
    }
  }

  declineDeviceMotionTracking = () => {
    this.setState((state, props) => ({ displayPermissionRequest: false }));
  }

  handleMotionEvent = (deviceMotion) => {
    let transformedRotationAxisVector = this.orientationHandler.getRotatedVector(
      [deviceMotion.rotationRate.alpha, deviceMotion.rotationRate.beta],
      window.orientation
    );

    (this.horizontalAxisGestureHandler.detectGesture(
      transformedRotationAxisVector[0],
      this.liftBottom,
      this.liftTop
    ))();
    (this.verticalAxisGestureHandler.detectGesture(
      transformedRotationAxisVector[1],
      this.liftRight,
      this.liftLeft
    ))();
  }

  getRoute = () => {
    return (window.location.href).substring((window.location.href).indexOf(':3000') + 6);
  }
}
