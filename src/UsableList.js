import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faExternalLinkAlt } from '@fortawesome/free-solid-svg-icons'

export default class UsableList {
  #list;
  #actionDescription;

  constructor(list, actionDescription) {
    this.#list = list;
    this.#actionDescription = actionDescription;
  }

  getList() {
    return this.#list;
  }

  getActionDescription() {
    return (
      <span>
        <FontAwesomeIcon
          icon={faExternalLinkAlt}
          className="mr-2"
        />{this.#actionDescription}
      </span>);
  }
}
