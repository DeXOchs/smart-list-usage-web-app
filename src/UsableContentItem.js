export default class UsableContentItem {

  imageUrl;
  title;
  description;
  content;

  constructor(imageUrl, title, description, content) {
    this.imageUrl = imageUrl;
    this.title = title;
    this.description = description;
    this.content = content;
  }

  getHeaderImageUrl() {
    return this.imageUrl
  };

  getTitle() {
    return this.title;
  }

  getTeaser() {
    return this.description;
  }

  getContent() {
    return this.content;
  }

  doPreferredAction() {
    alert('Action triggered: imagine anything ...');
  }
}
