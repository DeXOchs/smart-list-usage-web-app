import UsableList from './UsableList';
import UsableContentItem from './UsableContentItem';

export default class Service {
  getLatestNews() {
    return fetch('https://newsapi.org/v2/top-headlines?country=de&apiKey=b823b04416b3435c9a3af98296b812ee')
      .then(result => result.json())
      .then(rawJson => {
        return new UsableList(
          rawJson.articles.filter(article => (
            article.urlToImage != null &&
            article.title != null &&
            article.description != null &&
            article.content != null
          ))
            .map(article => new UsableContentItem(
              article.urlToImage,
              article.title,
              article.description,
              article.content
            )
          ),
          'Share'
        );
      });
  }

  getDemoImages() {
    return fetch('https://api.unsplash.com/photos/?query=nature&orientation=portrait&featured&per_page=20&client_id=231cfd601eef7769e9b1580e54c2600605b77fce38382d88c8a0c339a96984ea')
      .then(result => result.json())
      .then(rawJson => {
        return new UsableList(
          rawJson.filter(imageData => (
            imageData.description != null &&
            imageData.user.name != null &&
            imageData.user.links.self != null
          ))
            .map(imageData => new UsableContentItem(
              imageData.urls.raw + '&crop=edges&fit=clamp',
              imageData.description,
              'By: ' + imageData.user.name,
              'Visit User Profile under ' + imageData.user.links.self
            )
          ),
          'Share'
        )
      });
  }

  getDummyContent() {
    return new UsableList(
      [new UsableContentItem(
        './logo_1024.png',
        'No Service specified',
        'To display list like content, please specify service and/ or data source',
        ''
      )],
      ' Do nothing'
    )
  }
}
