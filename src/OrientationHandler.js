export default class OrientationHandler {
  getRotatedVector(twoDimVector, deviceRotation) {
    let rotation = [
      Math.cos(deviceRotation), -1 * Math.sin(deviceRotation),
      +1 * Math.sin(deviceRotation), Math.cos(deviceRotation)
    ];
    return [
      rotation[0]*twoDimVector[0] + rotation[1]*twoDimVector[1],
      rotation[2]*twoDimVector[0] + rotation[3]*twoDimVector[1]
    ]
  }
}
