export default class StatefulGestureHandler {

  buffersize;
  activationThreshold;
  buffer;

  constructor({buffersize, activationThreshold}) {
    this.buffersize = buffersize;
    this.activationThreshold = activationThreshold;
    this.buffer = [];
    this.counter = 0;
  }

  detectGesture(motionValue, onNegativeCallback, onPositiveCallback) {
    if (this.counter >= 0) {
      this.buffer.push(motionValue);
      if (this.buffer.length > this.buffersize) {
        this.buffer.shift();
      }
      let average = this.buffer.reduce((a, b) => (a + b)) / this.buffer.length;
      if (average < -this.activationThreshold) {
        this.setCooldown();
        return onNegativeCallback;
      } else if (average > +this.activationThreshold) {
        this.setCooldown();
        return onPositiveCallback;
      }
    } else {
      this.counter++;
    }
    return (() => {});
  }

  setCooldown() {
    this.buffer = [];
    this.counter = -50;
  }
}
