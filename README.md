# smart_list_usage_web_app
A React web app which can display list-like content and provides a detail view for list items. On devices with motion sensor, the user can control the web app with device gestures, which enables non-standard UX.

To use, simply start node server with npm and visit the provided web address with a modern browser.
